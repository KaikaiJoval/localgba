package republisys.globalbankaccess.module.presentation.login

import android.app.Activity
import android.content.Intent
import android.support.annotation.UiThread
import android.widget.Toast
import com.afollestad.materialdialogs.GravityEnum
import mcig.base.presentation.base.BasePresenterActivity
import mcig.base.presentation.base.BasePresenterFactory

import republisys.globalbankaccess.R
import republisys.globalbankaccess.databinding.ActivityLoginBinding
import republisys.globalbankaccess.module.data.entity.local.CountryEntity
import republisys.globalbankaccess.module.presentation.main.MainActivity
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by republisys on 9/25/17.
 */
class LoginActivity : BasePresenterActivity<ActivityLoginBinding, LoginViewModel, LoginPresenter, LoginView>(), LoginView {

    @Inject internal lateinit var loginPresenterFactory: LoginPresenterFactory

    override fun getLayoutId(): Int = R.layout.activity_login
    override fun injectComponents(): Activity = this

    override fun createPresenterFactory(): BasePresenterFactory<LoginPresenter> = loginPresenterFactory
    override fun createViewModel(): LoginViewModel = LoginViewModel()

    override fun setViewModel(binding: ActivityLoginBinding, viewModel: LoginViewModel) {
        binding.viewModel = viewModel
    }

    @UiThread
    override fun onPresenterCreated() {
        Timber.tag("lifeCycles")
        Timber.i("onPresenterCreated")
        presenter!!.fetchAll()
    }

    override fun onPresenterDestroyed() {
        Timber.i("onPresenterDestroyed")
    }

    override fun showMainActivity() {

        dialog {

            title {
                text = "Hello"
                color = R.color.cardview_shadow_end_color
            }

            message {
                text = "HAHAHAH"
                color = R.color.cardview_shadow_end_color
            }

            positive {
                text = "OK"
                color = R.color.cardview_shadow_end_color
            }

            negative {
                text = "CANCEL"
            }

            gravity(GravityEnum.START)

            onPositiveClick {
                Toast.makeText(applicationContext, "POSITIVE", Toast.LENGTH_LONG).show()
            }

            onNegativeClick {
                Toast.makeText(applicationContext, "NEGATIVE", Toast.LENGTH_LONG).show()
            }

        }.show()

    }
}