package republisys.globalbankaccess.module.presentation.login

import mcig.base.presentation.viewmodel.BaseViewModel
import mcig.base.util.validator.BaseValidator
import mcig.base.util.validator.ConstantValidator
import mcig.base.util.validator.ObservableValidatorField
import mcig.base.util.validator.ObservableValidatorForm
import mcig.base.util.validator.rules.NotSameAsValidator
import mcig.base.util.validator.rules.SameAsValidator
import javax.annotation.Nullable
import javax.inject.Inject

/**
 * Created by republisys on 10/10/17.
 */
class LoginViewModel @Inject constructor() : BaseViewModel() {

    @JvmField
    val usernameObservableValidator: ObservableValidatorField<String> = ObservableValidatorField(value = "", validator = ConstantValidator.PasswordValidator().build())

    @JvmField
    val passwordObservableValidator: ObservableValidatorField<String> = ObservableValidatorField(value = "", validator = ConstantValidator.PasswordValidator()
            .withRule(NotSameAsValidator(usernameObservableValidator, "password must not be the same of old password"))
            .withRule(object : BaseValidator<String> {

                override fun isValid(t: String?): Boolean {
                    val isValid = t != null && t.compareTo(confirmPasswordObservableValidator.value!!) == 0
                    if (isValid) {
                        confirmPasswordObservableValidator.hideErrorMessage()
                    } else {
                        confirmPasswordObservableValidator.showError(getErrorMessage())
                    }
                    return true
                }

                override fun getErrorMessage(): String {
                    return "Have to be the same as new password"
                }

            }).build())

    @JvmField
    var confirmPasswordObservableValidator = ObservableValidatorField(value = "", validator = ConstantValidator.PasswordValidator().withRule(SameAsValidator(passwordObservableValidator, "Have to be the same as new password")).build())

    var validForm = ObservableValidatorForm(usernameObservableValidator, passwordObservableValidator, confirmPasswordObservableValidator)

}