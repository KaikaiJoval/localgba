package republisys.globalbankaccess.module.domain

import android.os.Handler
import android.os.Looper
import android.support.annotation.NonNull
import io.reactivex.Flowable
import io.reactivex.functions.Function
import mcig.base.common.preconditions.Preconditions
import mcig.base.common.providers.TimestampProvider
import mcig.base.data.cache.Cache
import mcig.base.data.store.MemoryReactiveStore
import mcig.base.data.store.ReactiveStore
import mcig.base.data.store.Store
import mcig.base.domain.BaseEntity
import polanski.option.Option
import polanski.option.function.Func1
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledFuture

/**
 * Created by republisys on 10/2/17.
 */

abstract class CacheRepository<T : BaseEntity>{

    private val CACHE_MAX_AGE = (5 * 60 * 1000).toLong()

    private val timestampProvider = TimestampProvider()

    private val cacheStore: Store.MemoryStore<String, T>
    val memoryStore: ReactiveStore<String, T>

    private var scheduledFuture: ScheduledFuture<*>? = null

    init {
        this.cacheStore = Cache(Function { it.id() }, timestampProvider, CACHE_MAX_AGE)
        this.memoryStore = MemoryReactiveStore(Func1 { it.id() }, cacheStore)
    }

    @NonNull
    fun getAllFromCache(): Flowable<Option<List<T>>> {
        return this.memoryStore.all
    }

}
