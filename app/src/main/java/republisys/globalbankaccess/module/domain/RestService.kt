package republisys.globalbankaccess.module.domain

import io.reactivex.Single

import republisys.globalbankaccess.module.data.entity.local.CountryEntity
import republisys.globalbankaccess.module.data.entity.raw.CountryEntityRaw
import republisys.globalbankaccess.module.data.entity.raw.ResultRaw

import retrofit2.http.GET

/**
 * Created by republisys on 9/25/17.
 */

interface RestService{

    @GET("api/mobile/v1/getCountries")
    fun getCountries(): Single<ResultRaw<CountryEntityRaw>>

}