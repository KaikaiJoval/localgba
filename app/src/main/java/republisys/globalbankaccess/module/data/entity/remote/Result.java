package republisys.globalbankaccess.module.data.entity.remote;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.util.List;

/**
 * Created by republisys on 9/27/17.
 */

@AutoValue
public abstract class Result<T> {

    @SerializedName("status")
    public abstract int status();

    @SerializedName("data")
    public abstract List<T> dataList();

    @NonNull
    public static Builder builder() {
        return new $AutoValue_Result.Builder();
    }

    public static <T> TypeAdapter<Result<T>> typeAdapter(Gson gson, TypeToken<? extends Result<T>> typeToken) {
        return new AutoValue_Result.GsonTypeAdapter(gson, typeToken);
    }

    @AutoValue.Builder
    public interface Builder<T> {

        Builder<T> status(final int status);
        Builder<T> dataList(final List<T> dataList);

        Result<T> build();
    }

}
