package republisys.globalbankaccess.module.interactor

import android.support.annotation.NonNull
import android.support.v7.util.DiffUtil
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import mcig.base.common.rx.UnwrapOptionTransformer
import mcig.base.domain.ReactiveInteractor
import republisys.globalbankaccess.module.repository.UserRepository

import polanski.option.Option
import republisys.globalbankaccess.module.data.entity.local.CountryEntity
import javax.inject.Inject

/**
 * Created by republisys on 9/25/17.
 */

/**
 * Interactor is only concerned with those data access and processing calls that are required by the feature that it is serving.
 *
 * Method created in this class are Create, Read or Fetch, Update and Delete.
 *
 */
class UserEntityInteractor @Inject constructor(@NonNull val repository: UserRepository) :
        ReactiveInteractor.RetrieveInteractor<Void, List<CountryEntity>> {

    override fun getBehaviorStream(params: Option<Void>): Flowable<List<CountryEntity>> {
        return repository.getAll()
                .flatMapSingle { this.fetchWhenNoneAndThenDrafts(it) }
                .compose(UnwrapOptionTransformer.create())
    }

    private fun fetchWhenNoneAndThenDrafts(drafts: Option<List<CountryEntity>>): Single<Option<List<CountryEntity>>> {
        return fetchWhenNone(drafts).andThen(Single.just(drafts))
    }

    private fun fetchWhenNone(drafts: Option<List<CountryEntity>>): Completable {
        return if (drafts.isNone)
            repository.fetchData()
        else
            Completable.complete()
    }
}