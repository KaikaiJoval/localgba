package mcig.a420cloud.application.modules

import dagger.Module
import dagger.Provides
import mcig.base.data.store.ReactiveStore
import republisys.globalbankaccess.injection.module.DataModule

import republisys.globalbankaccess.module.domain.RestService
import republisys.globalbankaccess.module.repository.UserRepository
import republisys.globalbankaccess.injection.module.NetworkModule
import republisys.globalbankaccess.module.data.entity.local.CountryEntity
import republisys.globalbankaccess.module.data.entity.mapper.CountryEntityMapper

/**
 * Created by republisys on 7/18/17.
 */

@Module(includes = arrayOf(NetworkModule::class, DataModule::class))
class RepositoryBuilderModule {

    @Provides
    fun providedUserRepository(store: ReactiveStore<String, CountryEntity>, restService: RestService): UserRepository = UserRepository(store, CountryEntityMapper(), restService)

}
