package republisys.globalbankaccess.injection.component

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import republisys.globalbankaccess.GlobalBankAccess
import republisys.globalbankaccess.injection.module.ActivityBuilderModule
import republisys.globalbankaccess.injection.module.ApplicationModule
import republisys.globalbankaccess.injection.module.NetworkModule
import javax.inject.Singleton

/**
 * Created by republisys on 10/23/17.
 */

@Singleton
@Component(modules = arrayOf(ApplicationModule::class, AndroidInjectionModule::class, ActivityBuilderModule::class))
interface ApplicationComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent

    }

    fun inject(application: GlobalBankAccess)

}