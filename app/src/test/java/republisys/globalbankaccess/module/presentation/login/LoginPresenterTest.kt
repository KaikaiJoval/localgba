package republisys.globalbankaccess.module.presentation.login

import io.reactivex.Completable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Function
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.subscribers.TestSubscriber
import junit.framework.Assert.assertNotNull
import junit.framework.Assert.assertNull
import mcig.base.common.providers.TimestampProvider
import mcig.base.data.cache.Cache
import mcig.base.data.store.MemoryReactiveStore
import mcig.base.data.store.ReactiveStore
import mcig.base.data.store.Store
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.stubbing.OngoingStubbing
import polanski.option.Option
import polanski.option.function.Func1
import republisys.globalbankaccess.module.data.entity.local.CountryEntity
import republisys.globalbankaccess.module.data.entity.mapper.CountryEntityMapper
import republisys.globalbankaccess.module.domain.RestService
import republisys.globalbankaccess.module.interactor.SystemSettingsInteractor
import republisys.globalbankaccess.module.interactor.UserEntityInteractorTest
import republisys.globalbankaccess.module.repository.SystemSettingsRepository
import republisys.globalbankaccess.module.test_commom.BaseTest

/**
 * Created by republisys on 10/27/17.
 */

class LoginPresenterTest : BaseTest() {

    @Mock
    lateinit var store: ReactiveStore<String, CountryEntity>

    @Mock
    lateinit var restService: RestService

    @Mock
    lateinit var mapper: CountryEntityMapper

    @Mock
    lateinit var loginView: LoginView

    @Mock
    lateinit var  repository: SystemSettingsRepository

    @Mock
    lateinit var systemInteractor: SystemSettingsInteractor

    var loginPresenter: LoginPresenter? = null

    lateinit var testSubscriber: TestSubscriber<List<CountryEntity>>

    @Before
    fun setUp() {
        loginPresenter = LoginPresenter(systemInteractor)

        testSubscriber = TestSubscriber()
    }

    @Test
    fun testAttach() {
        assertNull(loginPresenter!!.view)

        loginPresenter!!.attachView(loginView!!)
        assertNotNull(loginPresenter!!.view)
    }

    @Test
    fun testDetach() {
        loginPresenter!!.detachView()
        assertNull(loginPresenter!!.view)
    }

    @Test
    fun testGetCountryList(){

        loginPresenter!!.fetchAll()

        Mockito.verify(loginView)!!.showMainActivity()

    }

}
