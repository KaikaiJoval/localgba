package republisys.globalbankaccess.module.repository

import io.reactivex.Flowable
import io.reactivex.Single

import mcig.base.data.store.ReactiveStore

import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import polanski.option.Option
import republisys.globalbankaccess.module.data.entity.local.CountryEntity
import republisys.globalbankaccess.module.domain.RestService
import republisys.globalbankaccess.module.test_commom.BaseTest

import org.assertj.core.api.AssertionsForClassTypes.assertThat
import republisys.globalbankaccess.module.data.entity.mapper.CountryEntityMapper
import republisys.globalbankaccess.module.data.entity.raw.CountryEntityRaw
import republisys.globalbankaccess.module.data.entity.raw.ResultRaw

import java.util.*

/**
 * Created by republisys on 10/25/17.
 */
class UserRepositoryTest : BaseTest() {

    @Mock
    lateinit var store: ReactiveStore<String, CountryEntity>

    @Mock
    lateinit var restService: RestService

    @Mock
    lateinit var mapper: CountryEntityMapper

    lateinit var repository: UserRepository

    @Before
    fun setUp(){
        repository = UserRepository(store, mapper, restService)
    }

    @Test
    fun getAllCountryReturnsStoreFlowable(){
        val storeFlowable: Flowable<Option<List<CountryEntity>>> = Flowable.empty()
        ArrangeBuilder().withFlowableFromStore(storeFlowable)
        assertThat(repository.getAll()).isEqualTo(storeFlowable)
    }

    @Test
    fun fetchCountryListEmitsNoErrorWhenNetworkServiceErrors(){
        val throwable = Mockito.mock(Throwable::class.java)
        ArrangeBuilder().withErrorInCountryListFromService(throwable)

        repository.fetchData().test().assertError(throwable)
    }

    @Test
    fun countryEntityItemsFromServiceAreMapped(){
        val rawList = createRawList()
        ArrangeBuilder().withCountryEntityFromService(rawList)
                .withMappedCountryEntity(Mockito.mock(CountryEntity::class.java))

        repository.fetchData().subscribe()

        Mockito.verify(mapper).apply(rawList[0])
        Mockito.verify(mapper).apply(rawList[1])
        Mockito.verify(mapper).apply(rawList[2])

    }

    @Test
    fun countryEntityAreStoredInStoreViaReplaceAll(){
        val safe = Mockito.mock(CountryEntity::class.java)

        ArrangeBuilder()
                .withCountryEntityFromService(Collections.singletonList(Mockito.mock(CountryEntityRaw::class.java)))
                .withMappedCountryEntity(safe)

        repository.fetchData().subscribe()

        Mockito.verify(store).replaceAll(Collections.singletonList(safe))
    }


    private fun createRawList(): List<CountryEntityRaw> {
        return object : ArrayList<CountryEntityRaw>() {
            init {
                add(Mockito.mock(CountryEntityRaw::class.java))
                add(Mockito.mock(CountryEntityRaw::class.java))
                add(Mockito.mock(CountryEntityRaw::class.java))
            }
        }
    }

    inner class ArrangeBuilder {

        fun withFlowableFromStore(flowable: Flowable<Option<List<CountryEntity>>>): ArrangeBuilder {
            Mockito.`when`(store.all).thenReturn(flowable)
            return this
        }

        fun withCountryEntityFromService(raws: List<CountryEntityRaw>): ArrangeBuilder {
            Mockito.`when`(restService.getCountries()).thenReturn(Single.just(object : ResultRaw<CountryEntityRaw>() {
                override fun status(): Int = 20
                override fun dataList(): List<CountryEntityRaw> = raws

            }))
            return this
        }

        fun withErrorInCountryListFromService(error: Throwable): ArrangeBuilder{
            Mockito.`when`(restService.getCountries()).thenReturn(Single.error(error))
            return this
        }

        fun withMappedCountryEntity(countryEntity: CountryEntity): ArrangeBuilder {
            Mockito.`when`(mapper.apply(Mockito.any())).thenReturn(countryEntity)
            return this
        }

    }

}
