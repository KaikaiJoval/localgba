package mcig.base.presentation.providers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.util.Pair;

import javax.inject.Inject;

import mcig.base.common.utils.StringUtils;

/**
 * Provides access to string resources to classes that have not access to Context (publishers, mappers, etc.)
 */
public class StringProvider {

    @NonNull
    private final Context context;

    @NonNull
    private final StringUtils stringUtils;

    @Inject
    StringProvider(@NonNull final Context context, @NonNull final StringUtils stringUtils) {
        this.context = context;
        this.stringUtils = stringUtils;
    }

    @NonNull
    public String getString(@StringRes final int resId) {
        return context.getString(resId);
    }

    @NonNull
    public String getString(@StringRes final int resId, @NonNull final Object... formatArgs) {
        return context.getString(resId, formatArgs);
    }

    /**
     * Use to replace the placeholders for strings that use the format "text {{placeholder}} text".
     *
     * @param stringResId   string resource id
     * @param substitutions substitutions
     * @return string
     */
    @SuppressWarnings("unchecked")
    public String getStringAndApplySubstitutions(@StringRes final int stringResId, @NonNull final Pair<String, String>... substitutions) {
        return stringUtils.applySubstitutionsToString(context.getString(stringResId), substitutions);
    }
}
