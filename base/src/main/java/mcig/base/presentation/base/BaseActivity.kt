package mcig.base.presentation.base

import android.app.Activity
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.afollestad.materialdialogs.MaterialDialog
import dagger.android.AndroidInjection
import estansaas.fonebayad.utils.DialogBuilder

/**
 * Created by republisys on 7/26/17.
 */

abstract class BaseActivity : AppCompatActivity() {

    private var isDialogNetworkCalled = false
    private var isEnableBinding = false

    public override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(injectComponents())
        super.onCreate(savedInstanceState)
    }

    abstract fun injectComponents(): Activity
    @LayoutRes abstract fun getLayoutId(): Int

    fun onError(error: String) {
        Log.e("ERROR", error)
    }


    fun dialog(init: DialogBuilder.() -> Unit): MaterialDialog {
        return DialogBuilder(this, init).build()
    }
}
