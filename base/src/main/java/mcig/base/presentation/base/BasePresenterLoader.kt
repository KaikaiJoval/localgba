package mcig.base.presentation.base

import android.content.Context
import android.support.v4.content.Loader

/**
 * Created by republisys on 7/17/17.
 */

class BasePresenterLoader<P : BasePresenter<*>> protected constructor(context: Context, private val factory: BasePresenterFactory<P>) : Loader<P>(context) {

    private var presenter: P? = null

    override fun onStartLoading() {
        if (null != presenter) {
            deliverResult(presenter)
            return
        }

        forceLoad()
    }

    override fun onForceLoad() {
        presenter = factory.create()

        deliverResult(presenter)
    }

    override fun onReset() {
        if (null != presenter) {
            presenter!!.detachView()
            presenter = null
        }
    }

    companion object {

        fun <P : BasePresenter<*>> newInstance(context: Context, factory: BasePresenterFactory<P>): Loader<P> {
            return BasePresenterLoader(context, factory)
        }
    }
}

