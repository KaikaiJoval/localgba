package mcig.base.util.validator

import mcig.base.util.validator.rules.Validator
import mcig.base.util.validator.rules.ValidatorRule

/**
 * Created by republisys on 10/12/17.
 */
class ValidatorBuilder<T>(private val validators: ArrayList<BaseValidator<T>>) : BaseValidator<T> {

    var error: String? = null

    override fun isValid(t: T?): Boolean {
        validators.forEach { validator ->
            if (!validator.isValid(t)) {
                error = validator.getErrorMessage()
                return false
            }
        }
        return true
    }

    override fun getErrorMessage(): String = error!!

    class Builder<T> {

        private var validators = arrayListOf<BaseValidator<T>>()

        fun build(): ValidatorBuilder<T> {
            return ValidatorBuilder(validators)
        }

        fun withRule(validator: BaseValidator<T>): Builder<T> {
            validators.add(validator)
            return this
        }

        fun withRule(validator: Validator<T>, error: String): Builder<T> {
            validators.add(ValidatorRule(validator, error))
            return this
        }
    }
}