package mcig.base.util.validator.rules

import mcig.base.util.validator.BaseValidator

/**
 * Created by republisys on 10/12/17.
 */
class ValidatorRule<T>(val validator: BaseValidator<T>, error: String) : Validator<T>(error) {

    override fun isValid(t: T?): Boolean = validator.isValid(t)

}