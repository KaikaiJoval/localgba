package mcig.base.common.rx

import java.io.IOException
import java.lang.reflect.Type

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

import mcig.base.domain.RetrofitException
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.HttpException
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory

/**
 * Created by republisys on 10/2/17.
 */

class RxErrorHandlingCallAdapterFactory private constructor() : CallAdapter.Factory() {

    private val original: RxJava2CallAdapterFactory = RxJava2CallAdapterFactory.create()

    override fun get(returnType: Type, annotations: Array<Annotation>, retrofit: Retrofit): CallAdapter<*, *>? {
        return RxCallAdapterWrapper(retrofit, original.get(returnType, annotations, retrofit) as CallAdapter<out Any, Any>)
    }

    private class RxCallAdapterWrapper<R> internal constructor(private val retrofit: Retrofit, private val wrapped: CallAdapter<R, Any>) : CallAdapter<R, Any> {

        override fun responseType(): Type {
            return wrapped.responseType()
        }

        override fun adapt(call: Call<R>): Any {
            val result = wrapped.adapt(call)

            if (result is Single<*>) {
                return result.onErrorResumeNext { throwable -> Single.error(throwable) }
            }

//            if (result is Observable<*>) {
//                return result.onErrorResumeNext { throwable -> Observable.error(throwable) }
//            }

            return if (result is Completable) {
                result.onErrorResumeNext { throwable -> Completable.error(asRetrofitException(throwable)) }
            } else result

        }

        private fun asRetrofitException(throwable: Throwable): RetrofitException {
            // We had non-200 http error
            if (throwable is HttpException) {
                val response = throwable.response()
                return RetrofitException.httpError(response.raw().request().url().toString(), response, retrofit)
            }
            // A network error happened
            return if (throwable is IOException) {
                RetrofitException.networkError(throwable)
            } else RetrofitException.unexpectedError(throwable)

        }
    }

    companion object {

        fun create(): CallAdapter.Factory {
            return RxErrorHandlingCallAdapterFactory()
        }
    }
}
