package mcig.base.data.store

import android.support.annotation.NonNull
import io.reactivex.Flowable
import io.reactivex.processors.FlowableProcessor
import io.reactivex.processors.PublishProcessor
import io.reactivex.schedulers.Schedulers
import polanski.option.Option
import polanski.option.Option.none
import polanski.option.Option.ofObj
import polanski.option.function.Func1
import java.util.*

/**
 * This reactive store has only a memory cache as form of storage.
 */
class MemoryReactiveStore<Key, Value>(private val extractKeyFromModel: Func1<Value, Key>,
                                      private val cache: Store.MemoryStore<Key, Value>) : ReactiveStore<Key, Value> {

    private val allProcessor: FlowableProcessor<Option<List<Value>>> = PublishProcessor.create<Option<List<Value>>>().toSerialized()

    private val processorMap = HashMap<Key, FlowableProcessor<Option<Value>>>()

    override fun storeSingular(@NonNull model: Value) {
        val key = extractKeyFromModel.call(model)
        cache.putSingular(model)
        getOrCreateSubjectForKey(key).onNext(ofObj(model))
        // One item has been added/updated, notify to all as well
        val allValues = cache.all.map { Option.ofObj(it) }.blockingGet(none())
        allProcessor.onNext(allValues)
    }

    override fun storeAll(modelList: List<Value>) {
        cache.putAll(modelList)
        allProcessor.onNext(ofObj(modelList))
        // Publish in all the existing single item streams.
        // This could be improved publishing only in the items that changed. Maybe use DiffUtils?
        publishInEachKey()
    }

    override fun replaceAll(modelList: List<Value>) {
        cache.clear()
        storeAll(modelList)
    }

    override fun getSingular(key: Key): Flowable<Option<Value>> {
        val model = cache.getSingular(key).map { Option.ofObj(it) }.blockingGet(none())
        return getOrCreateSubjectForKey(key).startWith(model)
                .observeOn(Schedulers.computation())
    }

    override fun getAll(): Flowable<Option<List<Value>>> {
        val allValues = cache.all.map { Option.ofObj(it) }.blockingGet(none())
        return allProcessor.startWith(allValues)
                .observeOn(Schedulers.computation())
    }

    private fun getOrCreateSubjectForKey(key: Key): FlowableProcessor<Option<Value>> {
        return ofObj(processorMap[key]).orDefault { createAndStoreNewSubjectForKey(key) }
    }

    private fun createAndStoreNewSubjectForKey(key: Key): FlowableProcessor<Option<Value>> {
        val processor = PublishProcessor.create<Option<Value>>().toSerialized()
        processorMap.put(key, processor)
        return processor
    }

    /**
     * Publishes the cached data in each independent stream only if it exists already.
     */
    private fun publishInEachKey() {
        val keySet: Set<Key> = HashSet(processorMap.keys)
        for (key in keySet) {
            val value = cache.getSingular(key).map { Option.ofObj(it) }.blockingGet(none())
            publishInKey(key, value)
        }
    }

    /**
     * Publishes the cached value if there is an already existing stream for the passed key. The case where there isn't a stream for the passed key
     * means that the data for this key is not being consumed and therefore there is no need to publish.
     */
    private fun publishInKey(key: Key, model: Option<Value>) {
        val processor: FlowableProcessor<Option<Value>> = processorMap[key]!!
        ofObj(processor).ifSome { it -> it.onNext(model) }
    }
}
